Gabby Good and Noah Davis

Real-Time Twitter Hashtag Encyclopedia

The goal of this application is, as the name
suggests, to provide a resource for people to gain useful
insights on commonly hashtagged terms from Twitter. In
practice, a user enters a search term, and our application
gathers Tweets containing this term (in hashtagged form)
and analyzes their contents, including the Tweet body and
other hashtags within the Tweet. Through the analysis
of many Tweets, our application can gain insight on the
term and provide the user with real-time information and
context. Some insights our application provides include
the general sentiment associated with the hashtag, a
WordCloud visualization to show what the term means,
and other common hashtags tweeted with the hashtag you
search. We hope this application with give people a new
and exciting way to interact with Twitter while also gaining
new insights and observations on social nuances that may
not immediately present themselves with a simple Google
search.

All PHP files to host the website can be found in the Apache folder. All files that
create and insert into the Oracle SQL Database can be found in the Database folder.
All Python scripts that stream Tweets from Twitter and extract useful information from the tweets in our Database can be found in the Python Scripts folder. The shell script that
streams for the 50 top trending tweets and inserts those tweets and hashtags into the 
Database is GATHER.sh.
