#!/bin/sh

rm /home/ec2-user/social_sensing/twitter_encyclopedia/tags.dat
rm /home/ec2-user/social_sensing/twitter_encyclopedia/hashtags.csv
rm /home/ec2-user/social_sensing/twitter_encyclopedia/tweets.csv

echo Getting Trending Topics...
python /home/ec2-user/social_sensing/twitter_encyclopedia/getTrends.py

echo Getting Tweets...
python /home/ec2-user/social_sensing/twitter_encyclopedia/streamTweets.py /home/ec2-user/social_sensing/twitter_encyclopedia/tags.dat

echo Loading Tweets into database...
/u01/app/oracle/product/11.2.0/xe/bin/sqlldr guest/guest control="/home/ec2-user/social_sensing/twitter_encyclopedia/tweets.ctl", errors=1000

echo Loading hashtags into database...
/u01/app/oracle/product/11.2.0/xe/bin/sqlldr guest/guest control="/home/ec2-user/social_sensing/twitter_encyclopedia/hashtags.ctl", errors=1000

#echo Generating wordclouds...
#python generate_clouds.py

echo Done!
