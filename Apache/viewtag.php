<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Twitter Encyclopedia</title>

  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

</head>

<body>

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top">
    <div class="container">
      <a href="http://34.224.122.69:8006/"><img src="tweebird.png" height=50px width=50px></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" href="index.php">Home
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="about.html">About</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <!-- Page Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <h1 class="mt-5">Twitter Hashtag Encyclopedia</h1>
        <p class="lead">Real-time definitions of the world's hottest topics!</p>
        <ul class="list-unstyled">
	
	<?php
		
		
		putenv("ORACLE_HOME=/u01/app/oracle/product/11.2.0/xe/");
		putenv("LD_LIBRARY_PATH=/u01/app/oracle/product/11.2.0/xe/lib");
		$tag = htmlspecialchars($_GET["tag"]);
		print "<font size='+3'>#" . $tag . "</font><br>";

		$command = escapeshellcmd('python get_definition.py ' . $tag);
		$output = shell_exec($command);

		$output = (float)$output;
	
		print "General Sentiment: ";	
		if ($output > 0.35) {
			print "overwhelmingly positive <font size='+2'>\xF0\x9F\x98\x81</font>";
		} else if ($output > 0.2) {
			print "very positive <font size='+2'>\xF0\x9F\x98\x83</font>";
		} else if ($output > 0) {
			print "moderately positive <font size='+2'>\xF0\x9F\x99\x82</font>";
		} else if ($output > -0.2) {
			print "moderately negative <font size='+2'>\xF0\x9F\x98\x90</font>";
		} else if ($output > -0.35) {
			print "very negative <font size='+2'>\xF0\x9F\x98\xA0</font>";
		} else {
			print "overwhelmingly negative <font size='+2'>\xF0\x9F\x98\xA1</font>";
		}
	
		print "<br>";
		
		print "<img src='/clouds2/" . $tag . ".png'>";

		$command = escapeshellcmd('python pop_tags.py ' . $tag);
		$output = shell_exec($command);
		
		print "<br><br>";
			
		$tags = explode(' ', $output);
		print "<font size='+2'> Most Common Associated Tags: </font><br>";
		print "<a href=viewtag.php?tag=" . $tags[0] . ">#" . $tags[0] . "</a>";
		print "   ";
		print "<a href=viewtag.php?tag=" . $tags[1] . ">#" . $tags[1] . "</a>";
		print "   ";
		print "<a href=viewtag.php?tag=" . $tags[2] . ">#" . $tags[2] . "</a>";

	?>
	</ul>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>

</html>
