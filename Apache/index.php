<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Hastag Encyclopedia</title>

  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

</head>

<body>

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top">
    <div class="container">
      <img src="tweebird.png" width=50px height=50px> 
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="">Home
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="about.html">About</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <!-- Page Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <h1 class="mt-5">Twitter Hashtag Encyclopedia</h1>
        <p class="lead">Real-time definitions of the world's hottest topics!</p>
        <ul class="list-unstyled">
	<form action="/viewtag.php">
	<input type="text" placeholder="Enter a hashtag" name="tag">
	<button type="submit">Search</button>
	</form>
	<br>
	<?php
  		putenv("ORACLE_HOME=/u01/app/oracle/product/11.2.0/xe/");
  		$conn = oci_connect("guest", "guest", "xe")
  		   or die("<br>Couldn't connect");

  		$query = "select * from (select tag from hashtags group by tag order by count(*) desc) where rownum <= 15";
	

 		$stmt = oci_parse($conn, $query);
  		#oci(define_by_name($stmt, "TAG", $t);
  		$r = oci_execute($stmt);
  
  		if (!$r) {
			echo("error!\n");
  		}

  		#print "<table border='1'.\n";
  		while ($row = oci_fetch_array($stmt, OCI_ASSOC+OCI_RETURN_NULLS)) {
  			#print "<tr>\n";
			foreach ($row as $item) {
				$link_address = "http://34.224.122.69:8006/viewtag.php?tag=" . $item;
				print "<a class='col-lg-12 text-center' href='".$link_address."'>" . ($item !== null ? htmlentities($item, ENT_QUOTES) : "&nbsp;") . "</a></br>\n";
			}
			#print "</tr>\n";
  		}
  		#print "</table>\n";
  		oci_free_statement($stmt);
  		oci_close($conn);
	?>
	</ul>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>

</html>
