import tweepy
import requests
import os
import sys
import io
import csv
import re
import time
from requests_oauthlib import OAuth1

consumer_key = 'Vok5y7PSqd9sFpEjDDqLrP7WF'
consumer_secret = '4j0Fnj2V1jYaYMcwL2VuAgLGeccesJwAHw20nmap8ZhdckPmSL'

access_token = '748580990-vsG6QcCQtx1DFVZ7h2PPNcl0TdPivwoMkJSxBrfR'
access_token_secret = 'Yc7lrMdMLoL846MWthZDvFjSisoAOI6z5MbVLsSAgBg6h'

auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)

api = tweepy.API(auth)

class MyStreamListener(tweepy.StreamListener):
        def __init__(self, tweet_limit=15, time_limit=600):
                self.count = 0
                self.tweet_limit = tweet_limit
                self.start_time = time.time()
                self.time_limit = time_limit
                super(MyStreamListener, self).__init__()

        def on_status(self, data):
                if self.count < self.tweet_limit and (time.time() - self.start_time) < self.time_limit and data.text.encode('utf-8')[:2] != 'RT':
                        if (self.count % 100 == 0):
                            print("100 down baby!")
                        file_name = "/home/ec2-user/social_sensing/twitter_encyclopedia/tweets.csv"
                        with open(file_name, mode='a') as f:
                                with open("/home/ec2-user/social_sensing/twitter_encyclopedia/hashtags.csv", mode='a') as f1:
                                    writer = csv.writer(f1, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                                    for tag in data.entities['hashtags']:
                                        writer.writerow([tag['text'].encode('utf-8'), data.id])
                                writer = csv.writer(f, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                                body = data.text.encode('utf-8')
                                index = body.find('https://t.co/')
                                if index != -1:
                                    body = body[:index]
                                writer.writerow([data.id, body, data.created_at])
                        self.count += 1
                elif data.text.encode('utf-8')[:2] == 'RT':
                    pass
                else:
                        return False

        def on_error(self, status_code):
                print("error with code: " + repr(status_code))
                return True

if __name__ == "__main__":
    if len(sys.argv) == 1:
        print('Usage:')
        print('python streamTweets.py trendfile(default is tags.dat)')
    else:
        with open(sys.argv[1], mode='r') as hashtags:
            hashtags = hashtags.readlines()
            hashtags = [x.rstrip() for x in hashtags] 
            stream_listener = MyStreamListener(tweet_limit=10000)
            stream = tweepy.Stream(auth = auth, listener=stream_listener) 
            stream.filter(track=hashtags)
