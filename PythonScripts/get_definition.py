import cx_Oracle
import sys
from textblob import TextBlob
from collections import Counter

try:
    connection = cx_Oracle.connect("guest", "guest", "localhost/xe")

except cx_Oracle.DatabaseError, exception:
    print("failed")

cursor = connection.cursor()


hashtags = list()

tag = sys.argv[1].strip()

#print("TAG:")
#print(tag)
cursor.execute('select * from tweets natural join (select * from hashtags where tag = :hashtag)', hashtag=tag)

bodies = list()

numtweets = 0

for tweet in cursor:
    numtweets += 1
    newtweet = ''
    subtweet = tweet[1]
    for char in subtweet:
        if char not in "!@#,./'" and ord(char) < 128:
            newtweet += char
    #bodies.append(newtweet.decode(encoding='UTF-8', errors='replace'))
    bodies.append(newtweet)

#print("NUM TWEETS: ")
#print(numtweets)
text = " ".join(bodies)
#print(text)
blob = TextBlob(text)
print(blob.sentiment.polarity)
#print(max(blob.word_counts, key=blob.word_counts.get))
