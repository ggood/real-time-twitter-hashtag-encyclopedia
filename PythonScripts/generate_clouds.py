#!/usr/bin/env python

import cx_Oracle
import sys
import os
from wordcloud import WordCloud, STOPWORDS, ImageColorGenerator
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt

print("IN GENERATE CLOUDS")
os.environ['ORACLE_HOME'] = "/u01/app/oracle/product/11.2.0/xe/"
os.environ['LD_LIBRARY_PATH'] = "/u01/app/oracle/product/11.2.0/xe/lib/"


try:
    connection = cx_Oracle.connect("guest", "guest", "localhost/xe")

except cx_Oracle.DatabaseError, exception:
    print("failed")
    print(exception)

cursor = connection.cursor()

hashtags = list()

tag = sys.argv[1]

cursor.execute('select * from tweets natural join (select * from hashtags where tag = :hashtag)', hashtag=tag)

bodies = list()

for tweet in cursor:
    #tweet = body[0].decode(encoding='UTF-8', errors='replace');
    
    newtweet = ''
    words = list()
    subtweet = tweet[1]
    for char in subtweet:
        if char not in "!@#,./'" and ord(char) < 128:
            newtweet += char
    for word in newtweet.split(' '):
        if word.lower() != tag.lower():
            words.append(word)
    newtweet = " ".join(words)
    bodies.append(newtweet.decode(encoding='UTF-8', errors='replace'))

text = " ".join(bodies)

wordcloud = WordCloud().generate(text)
filename = "/var/www/html/clouds/" + tag + ".png"
print(filename)
wordcloud.to_file(filename)

f = open("testfile", "w+")
f.write("hiiii")
f.close()
