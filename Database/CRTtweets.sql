create table tweets
        (id int not null primary key,
        body varchar2(300) not null,
        timestamp date not null);
