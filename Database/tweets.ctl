load data
infile '/home/ec2-user/social_sensing/twitter_encyclopedia/tweets.csv' "str '\r\n'"
append into table tweets
fields terminated by "," optionally enclosed by '"'
(ID, BODY, TIMESTAMP DATE "YYYY-MM-DD HH24:MI:SS")
