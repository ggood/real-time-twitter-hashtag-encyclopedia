create table hashtags
        (tag varchar(255) not null,
        id int not null,
	constraint fk_id foreign key (id) references tweets(id) on delete cascade,
	constraint PK_hashtag primary key(tag, id));
